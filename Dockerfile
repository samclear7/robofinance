FROM openjdk:11

VOLUME /tmp
ADD /build/libs/robofinance-0.0.1-SNAPSHOT.jar robofinance-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","robofinance-0.0.1-SNAPSHOT.jar"]