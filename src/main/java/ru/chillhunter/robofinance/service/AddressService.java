package ru.chillhunter.robofinance.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.chillhunter.robofinance.dao.AddressRepository;
import ru.chillhunter.robofinance.domain.Address;

@Service
@Transactional
public class AddressService {

	@Autowired
	private AddressRepository addressRepository;

	public Address findById(Long id) {
		
		Optional<Address> address = addressRepository.findById(id);
		return address.isPresent() ? address.get() : null;
	}

	public void save(Address address) {
		addressRepository.save(address);
	}

}