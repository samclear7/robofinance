package ru.chillhunter.robofinance.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import ru.chillhunter.robofinance.support.Sex;

@Converter(autoApply = true)
public class SexConverter implements AttributeConverter<Sex, String> {

    @Override
    public String convertToDatabaseColumn(Sex attribute) {
        return attribute.name().toLowerCase();
    }

    @Override
    public Sex convertToEntityAttribute(String dbData) {
        return Sex.valueOf(dbData.toUpperCase());
    }

}