package ru.chillhunter.robofinance.support;

public enum Sex {
    MALE, FEMALE
}