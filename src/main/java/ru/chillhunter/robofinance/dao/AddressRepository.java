package ru.chillhunter.robofinance.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ru.chillhunter.robofinance.domain.Address;

@Repository
public interface AddressRepository extends CrudRepository<Address, Long> {}