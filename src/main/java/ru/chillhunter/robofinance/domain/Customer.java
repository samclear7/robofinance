package ru.chillhunter.robofinance.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

import ru.chillhunter.robofinance.converters.SexConverter;
import ru.chillhunter.robofinance.support.Sex;

@Getter
@Setter
@Entity
@Table(name="customer")
public class Customer {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@OneToOne(cascade = CascadeType.ALL)
	@NotNull(message = "You should specify registred address")
	@JoinColumn(name = "registred_address_id", referencedColumnName = "id", nullable = false)
	private Address registredAddress;

	@OneToOne(cascade = CascadeType.ALL)
	@NotNull(message = "You should specify actual address")
	@JoinColumn(name = "actual_address_id", referencedColumnName = "id", nullable = false)
	private Address actualAddress;

	@Column(name="first_name")
	@NotNull(message = "You should specify first name")
	private String firstName;

	@Column(name="last_name")
	private String lastName;

	@Column(name="middle_name")
	private String middleName;

	@Column(name="sex")
	@NotNull(message = "You should specify sex")
	@Convert(converter = SexConverter.class)
	private Sex sex;
}
