package ru.chillhunter.robofinance;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

import ru.chillhunter.robofinance.converters.SexConverter;
import ru.chillhunter.robofinance.support.Sex;

@SpringBootTest
class RobofinanceApplicationTests {

	@Autowired
	ApplicationContext context;

	@Test
	void contextLoads() {}

	@Test
	void isSexConverterWorksFine() {
		
		SexConverter converter = new SexConverter();
		
		assertEquals(converter.convertToDatabaseColumn(Sex.FEMALE), "female");
		assertEquals(converter.convertToDatabaseColumn(Sex.MALE), "male");
	}

	@Test
	void isThereIsNothingToTestHere() {

		boolean somethingToTest = false;
		assertFalse(somethingToTest);
	}

}
